import { useRef, useState,useEffect} from "react";
import{faCheck, faTimes,faInfoCircle} from "@fortawesome/free-solid-svg-icons"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const name_REGEX = /^[a-zA-Z][a-zA-Z_]{2,50}$/;
const email_REGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
const zip_REGEX = /(^\d{5}$)/;

const Form = () =>{

    const userRef = useRef();
    const errRef = useRef();

    const [name, setName] = useState('');
    const [validName, setValidName] = useState(false);
    const [nameFocus, setNameFocus] = useState(false);

    const [email, setEmail] = useState('');
    const [validEmail, setValidEmail] = useState(false);
    const [emailFocus, setEmailFocus] = useState(false);

    const [zipCode, setZipCode] = useState('');
    const [validZipCode, setValidZipCode] = useState(false);
    const [zipCodeFocus, setZipCodeFocus] = useState(false);

    const [errorMessage, setErrorMessage] = useState('');
    const [success, setSuccess] = useState(false);

    useEffect(() => {
        userRef.current.focus();
    }, [])

    useEffect(() => {
        const result = name_REGEX.test(name);
        setValidName(result);
    },[name])

    useEffect(() => {
        const result = email_REGEX.test(email);
        setValidEmail(result);
    },[email])

    useEffect(() => {
        const result = zip_REGEX.test(zipCode);
        setValidZipCode(result);
    },[zipCode])

    useEffect(() => {
        setErrorMessage('');
    },[name,email,zipCode] )

    const handleSubmit = async (e) => {
        e.preventDefault();
        setSuccess(true);
    }

  return (
    <>
    {success ? (
        <section>
            <h1>Sucess!</h1>
            <p>Name: {name} </p>
            <p>Email: {email}</p>   
            <p>Zip Code: {zipCode}</p>    
        </section>
    ) : (
    
    <section>
        <p ref = {errRef} className = {errorMessage ? "errorMessage" : "offscreen"} aria-live = "assertive">{errorMessage}</p>
        <h1> Customer Form</h1>
        <form onSubmit = {handleSubmit}>
            <label htmlFor = "name">
                Name:
                <span className = {validName ? "valid" : "hide"}>
                    <FontAwesomeIcon icon = {faCheck} />
                </span>
                <span className = {validName || !name ? "hide" : "invalid"}>
                    <FontAwesomeIcon icon = {faTimes} />
                </span>
            </label>
            <input
                type = "text"
                id = "name"
                ref = {userRef}
                autoComplete = "off"
                onChange = {(e) => setName(e.target.value)}
                required
                aria-invalid = {validName ? "false":"true"}
                aria-describedby = "uidnote"
                onFocus = {() => setNameFocus(true)}
                onBlur = {() => setNameFocus(false)}
            />
            <p id ="uidnote" className = {nameFocus && name && !validName ? "instructions" : "offscreen"}>
                <FontAwesomeIcon icon ={faInfoCircle} />
                Must be at least 3 characters
            </p>

            <label htmlFor = "email">
                Email:
                <span className = {validEmail ? "valid" : "hide"}>
                    <FontAwesomeIcon icon = {faCheck} />
                </span>
                <span className = {validEmail || !email ? "hide" : "invalid"}>
                    <FontAwesomeIcon icon = {faTimes} />
                </span>
            </label>
            <input
                type = "text"
                id = "email"
                ref = {userRef}
                autoComplete = "off"
                onChange = {(e) => setEmail(e.target.value)}
                required
                aria-invalid = {validEmail ? "false":"true"}
                aria-describedby = "emailnote"
                onFocus = {() => setEmailFocus(true)}
                onBlur = {() => setEmailFocus(false)}
            />
            <p id ="emailnote" className = {emailFocus && email && !validEmail ? "instructions" : "offscreen"}>
                <FontAwesomeIcon icon ={faInfoCircle} />
                Must be a valid email
            </p>

            <label htmlFor = "zipCode">
                ZipCode:
                <span className = {validZipCode ? "valid" : "hide"}>
                    <FontAwesomeIcon icon = {faCheck} />
                </span>
                <span className = {validZipCode || !email ? "hide" : "invalid"}>
                    <FontAwesomeIcon icon = {faTimes} />
                </span>
            </label>
            <input
                type = "text"
                id = "zipCode"
                ref = {userRef}
                autoComplete = "off"
                onChange = {(e) => setZipCode(e.target.value)}
                required
                aria-invalid = {validZipCode ? "false":"true"}
                aria-describedby = "zipnote"
                onFocus = {() => setZipCodeFocus(true)}
                onBlur = {() => setZipCodeFocus(false)}
            />
            <p id ="zipnote" className = {zipCodeFocus && zipCode && !validZipCode ? "instructions" : "offscreen"}>
                <FontAwesomeIcon icon ={faInfoCircle} />
                Must be a valid zip code: XXXXX
            </p>

            <button disabled = {!validName || !validEmail || !validZipCode ? true : false}>Submit</button>
        </form>
    </section>
    )}
    </>
  )
}

export default Form;