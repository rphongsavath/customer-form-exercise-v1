## Project Name: 
Customer Form Exercise v1

## Project Decription: 
This application displays basic form for user input. Users are able to fill in the fields with valid information to be able to submit the form. Once the form is submitted, a screen will prompt with a sucess message and the input provided by the user.

## Deployment
1. Copy the application from the GitLab repository
2. Navigate to the application folder
3. Right click in the folder and select "Git Bash Here"
4. Navigate up one folder
5. Type 'npm start' in the Git Bash terminal and application should start local port 3000 in a seperate browser window. If port is already in use, type 'npx kill-port 3000' in the Git Bash terminal. Rerun 'npm start'.
6. Open application folder in Visual Studio to review code. 


## Author: Richy Phongsavath
